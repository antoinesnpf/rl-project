import random as rd
from utils import one_hot
import torch 
import gym
import random as rd

class ReplayBuffer:
    
    'create instance of replay buffer '

    def __init__(self, capacity, n_actions, state_size):
        self.position = 0
        self.n_actions = n_actions
        self.state_size = state_size    
        self.capacity = capacity
        self.init = True

    def push(self, state, action, reward, next_state):
        """Saves a transition."""

        state = torch.tensor(state).unsqueeze(0)
        action = one_hot(action, self.n_actions).unsqueeze(0)
        reward = torch.tensor([reward]).unsqueeze(0)

        if next_state is not None :
            next_state =  torch.tensor(next_state).unsqueeze(0)
            filter = torch.tensor([True]).unsqueeze(0)
        else : 
            next_state = torch.zeros((1, self.state_size))
            filter = torch.tensor([False]).unsqueeze(0)

        if self.init : 
            self.init = False
            self.memory_state = state
            self.memory_action = action
            self.memory_reward = reward
            self.memory_next_state = next_state
            self.memory_filter = filter

        elif len(self.memory_reward) >= self.capacity :
            self.memory_state[self.position] = state
            self.memory_action[self.position] = action
            self.memory_reward[self.position] = reward
            self.memory_next_state[self.position] = next_state
            self.memory_filter[self.position] = filter
            self.position += 1
            self.position = self.position % self.capacity

        else :
            self.memory_state = torch.cat([self.memory_state, state], dim = 0)
            self.memory_action = torch.cat([self.memory_action, action], dim = 0)
            self.memory_reward = torch.cat([self.memory_reward, reward], dim = 0)
            self.memory_next_state = torch.cat([self.memory_next_state, next_state], dim = 0)
            self.memory_filter = torch.cat([self.memory_filter, filter], dim = 0)


            
    def sample(self, batch_size):
        indexes = rd.sample(list(range(len(self.memory_state))), batch_size)
        return self.memory_state[indexes], self.memory_action[indexes], self.memory_reward[indexes], self.memory_next_state[indexes], self.memory_filter[indexes]

    def __len__(self):
        assert len(self.memory_state) == len(self.memory_action) == len(self.memory_reward) == len(self.memory_next_state) == len(self.memory_filter) 

        return len(self.memory_state)




if __name__ == '__main__' : 
    replay_buffer = ReplayBuffer(10, n_actions = 2, state_size=4)
    env = gym.make("CartPole-v1")
    n_step = 10
    state = env.reset()

    for _ in range(n_step) : 
        action = rd.randint(0,1)
        next_state, reward, done, _ = env.step(action)
        state = next_state
        if done : 
            next_state = None

        replay_buffer.push(state, action, reward, next_state)

    sample = replay_buffer.sample(5)
    assert len(replay_buffer) == n_step