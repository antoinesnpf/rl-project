import torch 
import random as rd
import numpy as np
from train_functions import policy


def one_hot(idx, size) : 
    res = torch.zeros((size, ))
    res[idx] = 1
    return res


def write_grid_search_results(filename, param_values, param_names, mean_score, std_score, init = False):

    with open(filename, 'r') as file:
        lines = file.readlines()

    with open(filename, 'w') as file:
        new_line = ''
        if init : 
            new_line += 'mean score\tstd\t'
            for name in param_names : 
                new_line += name + '\t'
            new_line+= '\n'

        new_line += f'{mean_score:.2f}\t{std_score:.2f}\t'
        for value in param_values : 
            new_line += f'{value}\t'
        new_line += '\n'
        file.writelines(lines)
        file.write(new_line)

def moving_average(a, n=3) :
    ret = np.cumsum(a, dtype=float)
    ret[n:] = ret[n:] - ret[:-n]
    return ret[n - 1:] / n

if __name__ == '__main__' : 
    filename = 'grid_search_test.txt'
    param_values = (0.95, 0.1, 0.95, 0.95, 5)
    param_names = ('a', 'b', 'c', 'd', 'e')
    mean_score = 10.6549687498465
    std_score = 1.6546512398
    write_grid_search_results(filename, param_values, param_names, mean_score, std_score, init = False)