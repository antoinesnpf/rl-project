import torch.nn as nn

class Net(nn.Module):

    def __init__(self, obs_size, hidden_size, n_actions):
        super().__init__()
        self.lin1 = nn.Linear(obs_size, hidden_size)
        self.relu = nn.ReLU()
        self.lin2 = nn.Linear(hidden_size, n_actions)
        
    def forward(self, x):
        x = self.lin1(x)
        x = self.relu(x)
        x = self.lin2(x)
        return x