import torch 
import random as rd
import numpy as np


def policy(state, epsilon, env, q_net, device):
    """
    Return action according to an epsilon-greedy exploration policy
    """
    if  rd.random() < epsilon:
        action = env.action_space.sample()
    else:
        with torch.no_grad() : 
            state = torch.tensor(state).to(device)
            action = torch.argmax(q_net(state)).item()
    return action


def eval(env, q_net, device, epsilon=0):
    state = env.reset()
    score = 0 

    while True :
        action = policy(state, epsilon, env, q_net, device)
        next_state, reward, done, _ = env.step(action)
        score += reward
        state = next_state

        if done:
            return score


def update(
    state, 
    action, 
    reward, 
    next_state, 
    done, 
    replay_buffer, 
    target_net, 
    q_net, 
    optimizer, 
    criterion, 
    batch_size, 
    device, 
    n_actions, 
    gamma
):

    'push new data to buffer'
    if done:
        next_state = None

    replay_buffer.push(state, action, reward, next_state)

    if len(replay_buffer) < batch_size:
        return np.inf

    ' update the qnet with gradient descent'
    state, action, reward, next_state, filter = replay_buffer.sample(batch_size)

    with torch.no_grad() : 
        target_values = target_net(next_state.to(device))

    optimizer.zero_grad()

    mask = torch.logical_not(filter.repeat(1, n_actions))
    target_values[mask] = 0
    target_values = torch.max(target_values, axis = 1)[0]
    targets = target_values * gamma + reward.to(device).squeeze(1)

    values = q_net(state.to(device))
    values = values[action.bool()]

    loss = criterion(values, targets)
    loss.backward()
    optimizer.step()
    
    return loss.item()



def train(
        env, 
        eval_env, 
        replay_buffer, 
        target_net, 
        q_net, 
        optimizer, 
        criterion, 
        scheduler, 
        batch_size, 
        device, 
        n_episodes, 
        n_actions, 
        gamma, 
        epsilon_start, 
        epsilon_min, 
        gamma_epsilon, 
        eval_every, 
        update_target_every,
        plot_every = 20
    ):

    'train the network for `n_episodes` and returns the scores acheved for each episode'

    q_net.to(device)
    target_net.to(device)


    state = env.reset()
    epsilon = epsilon_start
    ep = 0
    total_time = 0
    scores = []
    while ep < n_episodes:
        action = policy(state, epsilon, env, q_net, device)

        # take action and update replay buffer and networks
        next_state, reward, done, _ = env.step(action)
        loss = update(    
                state, 
                action, 
                reward, 
                next_state, 
                done, 
                replay_buffer, 
                target_net, 
                q_net, 
                optimizer, 
                criterion, 
                batch_size, 
                device, 
                n_actions, 
                gamma
            )

        # update state
        state = next_state

        # end episode if done
        if done:
            state = env.reset()
            ep += 1
            if ep % eval_every == 0:
                score = eval(eval_env, q_net, device)
                scores.append(score)
                if ep % plot_every == 0 :
                    print("episode =", ep+1, ", reward = ", score)

            # update target network
            if ep % update_target_every == 0:
                target_net.load_state_dict(q_net.state_dict())

            # decrease epsilon
            epsilon = epsilon_min + (epsilon - epsilon_min) * gamma_epsilon   
            scheduler.step()
        total_time += 1    

    return scores