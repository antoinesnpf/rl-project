import torch
import torch.nn as nn
import torch.optim as optim
import gym
from model import Net
from env import ReplayBuffer
import matplotlib.pyplot as plt
from train_functions import train, eval

# Environment
env = gym.make("CartPole-v1")
eval_env = gym.make("CartPole-v1")
device = torch.device('cuda:0')


# Hyperparameters
gamma = 0.95
learning_rate = 0.1  
gamma_scheduler = 0.999
gamma_epsilon = 0.99

batch_size = 100  
n_episodes = 400  
epsilon_start = 1.0   
epsilon_min = 0.05   
update_target_every = 5   
eval_every = 1

BUFFER_CAPACITY = 15000  
PLOT_EVERY = 20



# Neural nets and buffer
HIDDEN_SIZE = 128
OBS_SIZE = env.observation_space.shape[0]
N_ACTIONS = env.action_space.n
q_net = Net(OBS_SIZE, HIDDEN_SIZE, N_ACTIONS)
target_net = Net(OBS_SIZE, HIDDEN_SIZE, N_ACTIONS)
replay_buffer = ReplayBuffer(BUFFER_CAPACITY, n_actions=N_ACTIONS, state_size=OBS_SIZE)



# objective and optimizer
criterion = nn.MSELoss()
optimizer = optim.Adam(params=q_net.parameters(), lr=learning_rate)
scheduler = optim.lr_scheduler.ExponentialLR(optimizer, gamma=gamma_scheduler)



if __name__ == '__main__' : 
        REPEATS = 3
        les_scores = []
        plt.figure(figsize=(8,8))
        for i in range(REPEATS) : 
                q_net = Net(OBS_SIZE, HIDDEN_SIZE, N_ACTIONS)
                target_net = Net(OBS_SIZE, HIDDEN_SIZE, N_ACTIONS)
                replay_buffer = ReplayBuffer(BUFFER_CAPACITY, n_actions=N_ACTIONS, state_size=OBS_SIZE)



                # objective and optimizer
                criterion = nn.MSELoss()
                optimizer = optim.Adam(params=q_net.parameters(), lr=learning_rate)
                scheduler = optim.lr_scheduler.ExponentialLR(optimizer, gamma=gamma_scheduler)


                scores = train(
                        env, 
                        eval_env, 
                        replay_buffer, 
                        target_net, 
                        q_net, 
                        optimizer, 
                        criterion, 
                        scheduler, 
                        batch_size, 
                        device, 
                        n_episodes, 
                        N_ACTIONS, 
                        gamma, 
                        epsilon_start, 
                        epsilon_min, 
                        gamma_epsilon, 
                        eval_every, 
                        update_target_every ,
                        plot_every = 20
                )
                
                les_scores.append(scores)

                plt.plot(scores, label = f'training {i+1}')

        plt.title('evolution of the agent during training')
        plt.xlabel('episode number')
        plt.ylabel('score')
        plt.legend()
        plt.savefig('evolution of the agent during training.pdf')
        plt.show()
