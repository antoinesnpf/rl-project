import torch
import torch.nn as nn
import torch.optim as optim
import gym
import matplotlib.pyplot as plt
import numpy as np
import itertools
import time
import pickle

from model import Net
from env import ReplayBuffer
from utils import write_grid_search_results
from train_functions import train, eval


# Environment
env = gym.make("CartPole-v1")
eval_env = gym.make("CartPole-v1")
device = torch.device('cuda:0')


# Hyperparameters
gamma_ = [0.95, 0.99, 0.999]
learning_rate_ = [0.1, 0.01, 0.001]
gamma_scheduler_ = [0.95, 0.99, 0.999]
gamma_epsilon_ = [0.95, 0.99, 0.999]


batch_size = 256
n_episodes = 200  
epsilon_start = 1.0   
epsilon_min = 0.05
update_target_every = 5
eval_every = 1

BUFFER_CAPACITY = 2048
PLOT_EVERY = np.inf
REPEATS = 7

# Neural nets and buffer
HIDDEN_SIZE = 128
OBS_SIZE = env.observation_space.shape[0]
N_ACTIONS = env.action_space.n


if __name__ == '__main__' : 

    results = {}
    param_names = ('gamma', 'lr', 'gamma_sched', 'gamma_eps')
    n_params = len(list(itertools.product(
                    gamma_, 
                    learning_rate_, 
                    gamma_scheduler_, 
                    gamma_epsilon_
                )))

    for i, params in enumerate(itertools.product( 
                    gamma_, 
                    learning_rate_, 
                    gamma_scheduler_, 
                    gamma_epsilon_, 
        )) : 
        print('curent param:', params)

        gamma, learning_rate, gamma_scheduler, gamma_epsilon = params
        
        temp = []
        t = time.time()
        for _ in range(REPEATS) : 
            q_net = Net(OBS_SIZE, HIDDEN_SIZE, N_ACTIONS)
            target_net = Net(OBS_SIZE, HIDDEN_SIZE, N_ACTIONS)
            replay_buffer = ReplayBuffer(BUFFER_CAPACITY, n_actions=N_ACTIONS, state_size=OBS_SIZE)

            # objective and optimizer
            criterion = nn.MSELoss()
            optimizer = optim.Adam(params=q_net.parameters(), lr=learning_rate)
            scheduler = optim.lr_scheduler.ExponentialLR(optimizer, gamma=gamma_scheduler)

            # training

            scores = train(
                env, 
                eval_env, 
                replay_buffer, 
                target_net, 
                q_net, 
                optimizer, 
                criterion, 
                scheduler, 
                batch_size, 
                device, 
                n_episodes, 
                N_ACTIONS, 
                gamma, 
                epsilon_start, 
                epsilon_min, 
                gamma_epsilon, 
                eval_every, 
                update_target_every ,
                plot_every = PLOT_EVERY
            )

            temp.append(scores[int(n_episodes/10):])    

        temp = np.array(temp)
        mean_score = temp.mean()
        std_score = np.std(temp.mean(axis =1))
        print(f"""param vector [{i+1}/{n_params}] tested 
            duration : {time.time() - t:.2f}s 
            score : {mean_score:.2f} 
            std : {std_score:.2f}""")

        init = (i == 0)
        write_grid_search_results('grid_search.txt', params, param_names, mean_score, std_score, init = init)
        results[params] = (np.mean(temp), np.std(temp))


