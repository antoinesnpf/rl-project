
from abc import abstractmethod
from concurrent.futures.process import _process_worker
from lib2to3.pytree import Base
import os, sys
import gym
import time
import numpy as np
import text_flappy_bird_gym
from utils import argmax, plot_value_function
import matplotlib.pyplot as plt



class BaseAgent:
    def __init__(self, obs, eps) : 
        self.obs = obs
        self.eps = eps
        self.step_count = 0
        self.states = []
        self.actions = []
        self.rewards = []

        self.state_action_value_array = np.random.normal(size = (14, 25, 2))

        self.pi = np.random.uniform(low = 0, high = 1, size = (14, 25, 2))
        self.pi[:,:,0] = 1 - self.pi[:,:,1]

    def update_eps_greedy_policy(self):
        best_action = np.argmax(self.state_action_value_array, axis = 2)
        mask = (best_action == 0) #2D mask
        antimask = np.logical_not(mask) #2D
        mask = np.concatenate((mask[:,:,None], antimask[:,:,None]), axis = 2)
        self.pi[mask] = 1 - self.eps
        self.pi[np.logical_not(mask)] = self.eps 


    def policy(self, obs, save=True) : 
        x, y = obs
        if np.random.random() < self.pi[x, y , 0] : 
            action = 0
        else : 
            action = 1
        if save : 
            self.last_action = action
            self.obs = obs
        return action

    def get_reward(self, reward, next_obs, done):
        self.states.append(self.obs)
        self.actions.append(self.last_action)
        self.rewards.append(reward)
        self.next_obs = next_obs
        self.next_state_is_terminal = done
        self.step_count += 1 

    def reset_data(self) : 
        self.states = []
        self.rewards = []
        self.actions = []

    def update_episode(self) : 
        pass

    def update_step(self) : 
        pass
    
    def plot_q_function(self) : 
        V = {}
        for x in range(14) : 
            for y in range(-12, 13) : 
                for a, a_ in zip([False, True], [0,1]) : 
                    V[x, y, a] = self.state_action_value_array[x, y, a_]

        plot_value_function(V)
    
    def plot_pi(self) : 
        plt.figure()
        plt.title('policy probabilities for action 0 (Drop)')
        plt.imshow(self.pi[:,:,0])
        plt.colorbar()
        plt.show()

        plt.figure()
        plt.title('policy probabilities for action 1 (Jump)')
        plt.imshow(self.pi[:,:,1])
        plt.colorbar()
        plt.show()

class NoobAgent(BaseAgent):
    def __init__(self, obs) : 
        super().__init__(obs, 0)

    def policy(self, obs) : 
        x, y = obs
        if y > -1 : 
            action =  1 
        else : 
            action =  0
        self.last_action = action
        return action

class EpsGreedyAgent(BaseAgent): 

    def __init__(self, obs, eps = 0.1, learning_rate = None):
        super().__init__(obs, eps)
        self.learning_rate = learning_rate
        self.state_action_count = np.zeros((14, 25, 2))

    def get_reward(self, reward, next_obs, done) : 
        self.reward = reward 

    def update_step(self) :
        x, y = self.obs
        self.state_action_count[x, y, self.last_action] += 1 
        if self.learning_rate is None : 
            learning_rate = 1/self.state_action_count[x, y, self.last_action]
        else : 
            learning_rate = self.learning_rate
        self.state_action_value_array[x, y, self.last_action] +=  learning_rate * (self.reward - self.state_action_value_array[x, y, self.last_action])
        self.update_eps_greedy_policy()

class MonteCarloESAgent(BaseAgent):

    def __init__(self, obs, gamma = 0.9, eps = 0.1, eps_dr = 0.999):
        super().__init__(obs, 0)
        self.gamma = gamma
        self.eps = eps
        self.eps_dr = eps_dr
        self.points_removed = 0
        self.returns = self.init_returns()

    def init_returns(self) : 
        res = np.empty((14, 25, 2)).astype('object')
        for i in range(14) : 
            for j in range(25) :
                 res[i,j,0] = []
                 res[i,j,1] = []
        return res

    def update_episode(self) : 

        episode_list = list(zip(self.states, self.actions))
        episode_set = set(episode_list) 

        for (state, action) in episode_set: 
            first_occurence_idx = episode_list.index((state, action))

            G = sum([self.gamma**k * reward for (k, reward) in enumerate(self.rewards[first_occurence_idx:])])

            x, y = state
            self.returns[x, y, action].append(G)
            self.state_action_value_array[x, y, action] = np.mean(self.returns[x, y, action])
            best_action = argmax(self.state_action_value_array[x, y])

            self.pi[x, y, best_action] = 1 - self.eps
            self.pi[x, y, 1 - best_action] = self.eps

        self.reset_data()
        self.HP_update()
        
    def HP_update(self) : 
        self.eps *= self.eps_dr

class SarsaAgent(BaseAgent) : 
    def __init__(self, 
                obs, 
                eps = 0.1, 
                eps_dr = 0.999,
                alpha = 0.1, 
                alpha_dr = 0.999,
                gamma = .9) : 

        super().__init__(obs, eps)
        self.eps = eps
        self.eps_dr = eps_dr
        self.alpha = alpha
        self.alpha_dr = alpha_dr
        self.gamma = gamma


    def update_step(self):
        reward = self.rewards[-1]
        x, y = self.obs
        x_, y_ = self.next_obs
        next_action = self.policy(self.next_obs, save=False) 
        
        Q = self.state_action_value_array[x, y, self.last_action]
        Q_ = self.state_action_value_array[x_, y_, next_action] #sarsa
        
        self.state_action_value_array[x, y, self.last_action] += self.alpha*(reward + self.gamma*Q_ - Q)

        # update policy
        self.update_eps_greedy_policy() 

    def HP_update(self) : 
        self.eps *= self.eps_dr
        self.alpha *= self.alpha_dr

    def update_episode(self):
        self.reset_data()
        self.HP_update()
        
class QueueLearningAgent(BaseAgent) : 
    def __init__(self, 
                obs, 
                eps = 0.1, 
                eps_dr = 0.999,
                alpha = 0.1, 
                alpha_dr = 0.999,
                gamma = .9) : 

        super().__init__(obs, eps)
        self.eps = eps
        self.eps_dr = eps_dr
        self.alpha = alpha
        self.alpha_dr = alpha_dr
        self.gamma = gamma


    def update_step(self):
        reward = self.rewards[-1]
        x, y = self.obs
        x_, y_ = self.next_obs
        next_action = self.policy(self.next_obs, save=False) 
        
        Q = self.state_action_value_array[x, y, self.last_action]
        Q_ = np.max(self.state_action_value_array[x_, y_, :]) #queue learning
        
        self.state_action_value_array[x, y, self.last_action] += self.alpha*(reward + self.gamma*Q_ - Q)

        # update policy
        self.update_eps_greedy_policy() 

    def HP_update(self) : 
        self.eps *= self.eps_dr
        self.alpha *= self.alpha_dr

    def update_episode(self):
        self.reset_data()
        self.HP_update()

class nStepSarsaAgent(BaseAgent):
    def __init__(self, 
                obs, 
                n = 8, 
                eps = 0.1, 
                eps_dr = 0.999,
                alpha = 0.1, 
                alpha_dr = 0.999,
                gamma = .9) : 
        super().__init__(obs, eps)
        self.n = n
        self.eps = eps
        self.eps_dr = eps_dr
        self.alpha = alpha
        self.alpha_dr = alpha_dr
        self.gamma = gamma

        #init policy
        self.update_eps_greedy_policy() 

    def update_step(self):

        if len(self.rewards) >= self.n : 
            rewards = self.rewards[-self.n: ]
            states = self.states[-self.n: ]
            actions = self.actions[-self.n: ]

            powers = np.arange(self.n)
            rewards = np.array(rewards)
            gammas = np.array([self.gamma]*self.n)
            G = (gammas**powers * rewards).sum()
            if not self.next_state_is_terminal : 
                x_, y_ = self.next_obs
                G += self.gamma ** self.n * self.state_action_value_array[x_, y_, self.policy(self.next_obs, save=False)] 

            # update state value function
            x, y = states[0]
            action = actions[0]
            self.state_action_value_array[x, y, action] += self.alpha * (G - self.state_action_value_array[x, y, action])

            # update policy
            best_action = np.argmax(self.state_action_value_array, axis = 2)
            mask = (best_action == 0) 
            antimask = np.logical_not(mask) 
            mask = np.concatenate((mask[:,:,None], antimask[:,:,None]), axis = 2)

            self.pi[mask] = 1 - self.eps
            self.pi[np.logical_not(mask)] = self.eps 

    def HP_update(self) : 
        self.eps *= self.eps_dr
        self.alpha *= self.alpha_dr

    def update_episode(self):
        self.reset_data()
        self.HP_update()



if __name__ == '__main__':

    render = False
    #agent = NoobAgent(obs)

    for agent in [
        NoobAgent(None),
        EpsGreedyAgent(None), 
        MonteCarloESAgent(None), 
        SarsaAgent(None), 
        QueueLearningAgent(None),
        nStepSarsaAgent(None)
        ]: 

        # initiate environment
        env = gym.make('TextFlappyBird-v0', height = 15, width = 20, pipe_gap = 4)

        obs = env.reset()
        agent.obs = obs

        print(f'CURRENT AGENT IS : {agent.__class__}')
        n_iter_before_plot = 500000
        n_iter_after_plot = 0
        n_iter_total = n_iter_before_plot + n_iter_after_plot

        # iterate
        iter_nb = 0 
        game_count = 0
        max_scores = []
        while True:

            # Select next action
            action = agent.policy(obs)  

            # Appy action and return new observation of the environment
            obs, reward, done, info = env.step(action)
            agent.get_reward(reward, obs, done)
            agent.update_step() # agent is learning here

            # Render the game
            if render and (iter_nb > n_iter_before_plot) : 
                os.system("clear")
                sys.stdout.write(env.render())
                time.sleep(0.3)

            # If player is dead break
            if done: 
                obs = env.reset()
                agent.update_episode() # agent is learning here
                game_count += 1
                max_scores.append(info['score'])


            if iter_nb > n_iter_total :
                break

            iter_nb += 1

        env.close()

        plt.figure()
        plt.title(str(agent.__class__))

        plt.plot(np.arange(len(max_scores)), max_scores, 'x')
        plt.title(f'average score: {np.mean(max_scores):.2f}')
        plt.xlabel('game id')
        plt.ylabel('score')
        plt.show()

        agent.plot_pi()
        agent.plot_q_function()